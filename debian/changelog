ilorest (5.2.0.0-7) unstable; urgency=medium

  * Removed runtime depends on python3-setuptools (Closes: #1094413).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Jan 2025 09:09:55 +0100

ilorest (5.2.0.0-6) unstable; urgency=medium

  * Add fix-syntax-warning.patch (Closes: #1091466).

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Jan 2025 10:04:29 +0100

ilorest (5.2.0.0-5) unstable; urgency=medium

  * Really desactivate p3.13-fix-syntax-warning.patch (Closes: #1091466).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jan 2025 11:27:55 +0100

ilorest (5.2.0.0-4) unstable; urgency=medium

  * Deactivate p3.13-fix-syntax-warning.patch that's causing package to not be
    installable (Closes: #1091466).

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Dec 2024 10:20:14 +0100

ilorest (5.2.0.0-3) unstable; urgency=medium

  * Fixed d/watch to use mode=git and add pre-pending "v" char.
  * Add remove-intersphinx-from-doc.patch (Closes: #1090100).

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Dec 2024 13:42:07 +0100

ilorest (5.2.0.0-2) unstable; urgency=medium

  * Add p3.13-fix-syntax-warning.patch (Closes: #1085642).

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Dec 2024 14:58:06 +0100

ilorest (5.2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * (build-)depends on python3-ilorest >= 5.2.0.0.
  * Removed versions when satisfied in stable.
  * Rebased add-setup.py.patch.
  * Rebased fix-rdmc.py.patch.
  * Removed patches applied upstream:
    - fix-import-path.patch
    - pyproject.toml-add-triple-quotes-at-end-of-file.patch
  * Do not rm -f ilorest anymore: that's a folder now.
  * Add fix-py3-syntax-errors.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Sep 2024 14:23:12 +0200

ilorest (3.6.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1047908).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 15:17:53 +0200

ilorest (3.6.0.0-2) unstable; urgency=medium

  * Add missing python3-pyudev runtime depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Feb 2023 11:56:00 +0100

ilorest (3.6.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Upstream applied my import patch, so not using -ds* anymore.
  * Add add-setup.py.patch.
  * Symlink the src folder as ilorest to avoid rename.
  * (build-)depends on ilorest >= 3.6.0.0.
  * Fix pyproject.toml (Closes: #1022314).
  * Add fix-rdmc.py.patch.
  * Add fix-import-path.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Nov 2022 09:27:32 +0100

ilorest (3.5.1.0+ds3-1) unstable; urgency=medium

  * New upstream release.
  * (build-)depends on python3-ilorest >= 3.5.1.

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Aug 2022 10:21:46 +0200

ilorest (3.3.0.0+ds2-1) unstable; urgency=medium

  * New upstream release (ie: fixed git merge):
    - Fix Git merge conflict markers (Closes: #1004397).
  * Fix depends.
  * Fix wrong VCS fields.

 -- Thomas Goirand <zigo@debian.org>  Thu, 27 Jan 2022 09:30:29 +0100

ilorest (3.3.0.0+ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 17 Jan 2022 14:23:29 +0100

ilorest (3.2.2+ds1-3) unstable; urgency=medium

  * Add missing build-depends on python3-pbr (Closes: #1003870).

 -- Thomas Goirand <zigo@debian.org>  Mon, 17 Jan 2022 13:47:35 +0100

ilorest (3.2.2+ds1-2) unstable; urgency=medium

  * Rebuilt source-only.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 12:13:18 +0200

ilorest (3.2.2+ds1-1) unstable; urgency=medium

  * Initial release. (Closes: #994244)

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Sep 2021 10:23:17 +0200
